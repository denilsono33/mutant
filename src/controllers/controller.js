const service = require('../service/service')

module.exports.getAllUsers = async (req, res, next) => {


    try {
        let data = []

        await service.getAllUsers().then(contents => {
            data = contents
        })

        res.json({ data });
    } catch (error) {
        console.log(error)
    }
    
};

module.exports.getAllWebsites = async (req, res, next) => {

    try {
        let data = []

        await service.getAllWebsites().then(contents => {
            data = contents
        })

        res.json({ data });
    } catch (error) {
        console.log(error)
    }
    
};

module.exports.getUsersWithSuite = async (req, res, next) => {

    try {
        let data = []

        await service.getUsersWithSuite().then(contents => {
            data = contents
        })

        res.json({ data });
    } catch (error) {
        console.log(error)
    }
    
};
