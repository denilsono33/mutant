const axios = require('axios')
let service = {};

service.getAllUsers = async () => {

    const {data} = await axios({
        method: 'get',
        url: 'https://jsonplaceholder.typicode.com/users'
    })
    
    let dataFinal = data.map(item => {
        let newItem = {
            name: item.name,
            email: item.email,
            company: item.company.name
        }
    
        return newItem 
    })

    dataFinal = dataFinal.sort((a, b) => (a.name > b.name) ? 1 : -1)

    const response = {
        users: dataFinal
    }

    return response
      
}

service.getAllWebsites = async() => {

    const { data } = await axios({
        method: 'get',
        url: 'https://jsonplaceholder.typicode.com/users'
    })

    let dataFinal = data.map(item => {
        let newItem = {
            website: item.website
        }
    
        return newItem 
    })

    const response = {
        websites: dataFinal
    }

    return response

}

service.getUsersWithSuite = async() => {

    const { data } = await axios({
        method: 'get',
        url: 'https://jsonplaceholder.typicode.com/users'
    })

    let users_with_suite = []

    data.filter(element => element.address.suite.includes('Suite')).map(item => {
        users_with_suite.push({
            name: item.name,
            suite: item.address.suite
        })
    });

    const response = {
        users_with_suite
    }

    return response

}

module.exports = service;