const express = require('express');
const router = express.Router();
const controller = require('../controllers/controller')

router.get('/', function (req, res, next) {
    res.status(200).send({
        title: "Node Express API",
        version: "0.0.1"
    });
});



router.get('/getAllUsers', controller.getAllUsers);
router.get('/getAllWebsites', controller.getAllWebsites);
router.get('/getUsersWithSuite', controller.getUsersWithSuite);

module.exports = router;